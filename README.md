## Weatherbot NLU
- This repository supports an intent and entity recognition for a faq bot.

##How to run
python -m rasa_nlu.train -c config.yml -d intents --path faq_nlu

##How to train
python -m rasa_nlu.server --path faq_nlu --response_log data -w data

